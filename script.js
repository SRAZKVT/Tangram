let selectedItem = null

if (self ? self.chrome : browser) alert("Chrome / Chromium based browsers are not supported by this program. Switch to Mozilla Firefox or another supported browser. You can also ignore this message, but do know that Chrome and Chromium based browsers get no testing, and no bugs related to only Chromium based browsers will get fixed.")



setTimeout(() => document.getElementById("menu").childNodes.forEach(prepareMenuItem), 50)

function prepareMenuItem(elem) {
    let w = getElemSourceWidth(elem)
    elem.width = w * 6

    elem.addEventListener("click", _ => {
        const newShape = document.createElement("img")
        newShape.setAttribute("src", elem.getAttribute("src"))
        newShape.setAttribute("alt", elem.getAttribute("alt"))
        newShape.id = elem.getAttribute("alt")
        newShape.classList.add("moveable-item")
        newShape.width = getElemSourceWidth(elem) * 6

        newShape.ondragstart = () => { return false }

        newShape.addEventListener("mousedown", prepareClickItemInWorkspace, false)

        document.getElementById("workspace").appendChild(newShape)
        elem.parentElement.removeChild(elem)

        elem.remove()
    })
}

function prepareClickItemInWorkspace(clickEvent) {
    if (selectedItem !== null) {
        document.removeEventListener("mousemove", move)
        document.removeEventListener("keyup", rotate)

        // if dropped in menu
        if (selectedItem.objectX < 0) {
            // prepare menu option
            const menuOption = document.createElement("input")
            menuOption.classList.add("selection-shape")
            menuOption.type = "image"
            menuOption.src = selectedItem.src
            menuOption.alt = selectedItem.alt

            // put it in menu
            document.getElementById("menu").appendChild(menuOption)
            prepareMenuItem(menuOption)

            // remove moveable shape
            selectedItem.remove()
        }

        selectedItem = null
        return
    }
    selectedItem = this
    if (selectedItem.rotation === undefined) selectedItem.rotation = 0
    if (selectedItem.isFlipped === undefined) selectedItem.isFLipped = false

    updateItemTransform = () => {
        selectedItem.style.transform =
            `translate(${selectedItem.objectX}px, ${selectedItem.objectY}px)`
            + `rotate(${selectedItem.rotation}deg)`
            + `scaleX(${selectedItem.isFlipped ? -1 : 1})`
    }

    rotate = (e) => {
        let add = 0
        let flip = undefined
        if (e.key === 'r') add = 45
        if (e.key === 'f') flip = !selectedItem.isFlipped

        let newRotation = Math.abs((selectedItem.rotation + add) % 360)

        if (flip !== undefined) selectedItem.isFlipped = flip
        selectedItem.rotation = newRotation

        updateItemTransform()
    }

    move = (e) => {
        const size = getElemSourceSize(selectedItem)

        let newX = e.clientX - document.getElementById("menu").clientWidth - (size.width * 6 / 2)
        let newY = e.clientY - (size.height * 6 / 2)

        selectedItem.objectX = newX
        selectedItem.objectY = newY

        updateItemTransform()
    }

    document.addEventListener("keyup", rotate)
    document.addEventListener("mousemove", move, false)
}

function getElemSourceWidth(elem) {
    return getElemSourceSize(elem).width
}

function getElemSourceSize(elem) {
    const img = document.createElement('img')
    img.src = elem.src
    const size = { width: img.width, height: img.height }
    img.remove()
    return size
}