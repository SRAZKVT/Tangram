# Tangram

Small tangram thingie

## How to use

Click on pieces on the left, to bring them into the workspace. Once a piece is a part of the workspace, you can click it to select it. When a piece is
selected and you move your mouse, the piece will be moved along with it. If you press the 'r' key, the piece will be rotated by 45 degrees, and if you
press 'f', the piece will be flipped. When the piece is in its desired position, just click on it again to drop it.

## Supported browsers

I personally only use Firefox, so it's the only browser i support. Any bug from another browser will not be fixed, unless it also affects firefox.

### What if i don't want to / cannot use firefox anyway ?

One of the issues on Chromium based browsers i have found is if you don't select pieces by their center, then it will go way off. You can just press the
piece in its center then.
